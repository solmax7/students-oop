public interface IMarksCalculationService {

    public void AverageGroup(Group gr);

    public void AverageStudents(Group gr);

    public void NumberPerfectStudents(Group gr);

    public void NumberLoserStudents(Group gr);
}
