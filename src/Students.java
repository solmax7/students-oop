public class Students {

    private StudentProgress sp;
    private String name;
    private Group gr;

    public Students(String name, StudentProgress sp, Group gr){

        this.name = name;
        this.sp = sp;
        this.gr = gr;

    }

    public StudentProgress getSp() {
        return sp;
    }

    public void setSp(StudentProgress sp) {
        this.sp = sp;
    }

    public Group getGr() {
        return gr;
    }

    public void setGr(Group gr) {
        this.gr = gr;
    }

    public StudentProgress getSt() {
        return sp;
    }

    public void setSt(StudentProgress sp) {
        this.sp= sp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Students{" +
                "sp=" + sp +
                ", name='" + name + '\'' +
                ", gr=" + gr.getNameGroup() +
                '}';
    }
}
