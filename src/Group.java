import java.util.Arrays;

public class Group {

    private Students[] spisok = new Students[5];
    private String nameGroup;

    public Students[] getSpisok() {
        return spisok;
    }

    public void setSpisok(Students[] spisok) {
        this.spisok = spisok;
    }

    public String getNameGroup() {
        return nameGroup;
    }

    public void setNameGroup(String nameGroup) {
        this.nameGroup = nameGroup;
    }

    @Override
    public String toString() {
        return "Group{" +
                "spisok=" + Arrays.toString(spisok) +
                ", nameGroup='" + nameGroup + '\'' +
                '}';
    }
}
