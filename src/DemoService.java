public class DemoService {

  private MarksCalculationService marksCalculationService;

    public DemoService(MarksCalculationService marksCalculationService){

        this.marksCalculationService = marksCalculationService;
    }

    public void execute(){

        Group g1 = new Group();
        g1.setNameGroup("1 Gruppa");
        addGroup(g1);
        addMarksCalculationService(g1);

    }

    public void addStudents(){

    }

    public void addGroup(Group g1){

        Students[] s = new Students[5];
        g1.setSpisok(s);

        //int[] sp1 = new StudentProgress().addResultStudents();
        s[0] = new Students("Max", new StudentProgress(),g1);
        s[1] = new Students("Bill", new StudentProgress(),g1);
        s[2] = new Students("Margo", new StudentProgress(),g1);
        s[3] = new Students("Anna", new StudentProgress(),g1);
        s[4] = new Students("Fox", new StudentProgress(),g1);
        System.out.println(g1);
    }

    public void addMarksCalculationService(Group g1){

        marksCalculationService.AverageGroup(g1);

        marksCalculationService.AverageStudents(g1);

        marksCalculationService.NumberPerfectStudents(g1);

        marksCalculationService.NumberLoserStudents(g1);

    }


}
