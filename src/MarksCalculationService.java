public class MarksCalculationService implements IMarksCalculationService{

    public void AverageGroup(Group gr) {

        Students[] st = gr.getSpisok();

        double avg = 0;
        int sum = 0;
        int count = 0;

        for (int i = 0; i < gr.getSpisok().length; i++) {

            if (gr.getSpisok()[i] != null) {

                for (int j = 0; j < gr.getSpisok()[i].getSp().getResult().length; j++) {

                    int[] m = gr.getSpisok()[i].getSp().getResult();

                    if (m[j] != 0) {

                        sum = sum + m[j];
                        count++;

                    }

                }

            }
        }

        avg = (double) sum / (double) count;

        System.out.println("Средняя оценка группы: " + gr.getNameGroup() + " - " + avg);
        System.out.println();
     //   return avg;
    }

    public void AverageStudents(Group gr) {

        Students[] st = gr.getSpisok();

        System.out.println("Студенты группы: " + gr.getNameGroup());

        for (int i = 0; i < gr.getSpisok().length; i++) {

            double avg = 0;
            int sum = 0;
            int count = 0;

            if (gr.getSpisok()[i] != null) {

                for (int j = 0; j < gr.getSpisok()[i].getSp().getResult().length; j++) {

                    int[] m = gr.getSpisok()[i].getSp().getResult();

                    if (m[j] != 0) {

                        sum = sum + m[j];
                        count++;

                    }

                }
                avg = (double) sum / (double) count;
                System.out.println("Средняя оценка студента " + gr.getSpisok()[i].getName() + ": " + avg);

            }
        }
    }

    public void NumberPerfectStudents(Group gr) {

        int count = 0;

        for (int i = 0; i < gr.getSpisok().length; i++) {

            if (gr.getSpisok()[i] != null) {

                boolean flagPerfect = false;
                for (int j = 0; j < gr.getSpisok()[i].getSp().getResult().length; j++) {


                    int[] m = gr.getSpisok()[i].getSp().getResult();


                    if (m[j] != 5 && flagPerfect != true) {
                        flagPerfect = true;
                        break;

                    }
                }
                if (flagPerfect == false) {
                    count++;
                }
            }
        }
        System.out.println();
        System.out.println("Количество отличников: " + count);
    }

    public void NumberLoserStudents(Group gr) {

        int count = 0;

        for (int i = 0; i < gr.getSpisok().length; i++) {

            if (gr.getSpisok()[i] != null) {

             //   boolean flagPerfect = false;
                for (int j = 0; j < gr.getSpisok()[i].getSp().getResult().length; j++) {


                    int[] m = gr.getSpisok()[i].getSp().getResult();


                    if (m[j] < 3) {
                      //  flagPerfect = true;
                        count++;
                        break;

                    }
                }
            }

        }
        System.out.println();
        System.out.println("Количество с неудовлетворительными оценками: " + count);
    }

}
